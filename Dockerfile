FROM php:5.6.21-apache AS website
RUN apt update && \
	apt install -y nano libpng-dev && \
	a2enmod rewrite && \
	docker-php-ext-install mysqli pdo gd zip mbstring
ADD captcha captcha
RUN	["chmod", "-R", "777", "captcha"]
EXPOSE 80
